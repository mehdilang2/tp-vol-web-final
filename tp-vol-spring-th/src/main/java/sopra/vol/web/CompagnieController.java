package sopra.vol.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sopra.vol.Compagnie;
import sopra.vol.dao.ICompagnieRepository;


@Controller
@RequestMapping("/compagnie")
public class CompagnieController {
	@Autowired
	private ICompagnieRepository compagnieRepo;

	public CompagnieController() {
		super();

	}

	@GetMapping({ "", "/list" })
	public String list(Model model) {
		model.addAttribute("mesCompagnies", compagnieRepo.findAll());

		return "compagnie/list";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("maCompagnie", new Compagnie());

		return "compagnie/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam Long id, Model model) {
		Optional<Compagnie> optReservation = compagnieRepo.findById(id);

		if (optReservation.isPresent()) {
			model.addAttribute("maCompagnie", optReservation.get());
		}


		return "compagnie/form";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute("maCompagnie") @Valid Compagnie compagnie, BindingResult result, Model model) {

		if (result.hasErrors()) {

			return "compagnie/form";
		}

		compagnieRepo.save(compagnie);

		return "redirect:list";
	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {

		compagnieRepo.deleteById(id);

		return "redirect:/compagnie/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}
