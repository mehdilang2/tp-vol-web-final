package sopra.vol.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sopra.vol.Voyage;
import sopra.vol.dao.IVoyageRepository;


@Controller
@RequestMapping("/voyage")
public class VoyageController {
	@Autowired
	private IVoyageRepository voyageRepo;

	public VoyageController() {
		super();

	}

	@GetMapping({ "", "/list" })
	public String list(Model model) {
		model.addAttribute("mesVoyages", voyageRepo.findAll());

		return "voyage/list";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("monVoyage", new Voyage());

		return "voyage/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam Long id, Model model) {
		Optional<Voyage> optReservation = voyageRepo.findById(id);

		if (optReservation.isPresent()) {
			model.addAttribute("monVoyage", optReservation.get());
		}


		return "voyage/form";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute("monVoyage") @Valid Voyage voyage, BindingResult result, Model model) {

		if (result.hasErrors()) {

			return "voyage/form";
		}

		voyageRepo.save(voyage);

		return "redirect:list";
	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {

		voyageRepo.deleteById(id);

		return "redirect:/voyage/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}
