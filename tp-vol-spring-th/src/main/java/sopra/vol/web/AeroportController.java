package sopra.vol.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sopra.vol.Aeroport;
import sopra.vol.dao.IAeroportRepository;

@Controller
@RequestMapping("/aeroport")
public class AeroportController {
	@Autowired
	private IAeroportRepository aeroportRepo;

	@GetMapping({ "", "/list" })
	public String list(Model model) {
		model.addAttribute("mesAeroports", aeroportRepo.findAll());

		return "aeroport/list";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("monAeroport", new Aeroport());

		return "aeroport/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam String code, Model model) {
		Optional<Aeroport> optAeroport = aeroportRepo.findById(code);

		if (optAeroport.isPresent()) {
			model.addAttribute("monAeroport", optAeroport.get());
		}

		return "aeroport/form";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute("monAeroport") @Valid Aeroport aeroport, BindingResult result, Model model) {

		aeroportRepo.save(aeroport);

		return "redirect:list";

	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable String code) {

		aeroportRepo.deleteById(code);

		return "redirect:/aeroport/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}