package sopra.vol.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sopra.vol.Adresse;
import sopra.vol.Civilite;
import sopra.vol.Client;
import sopra.vol.ClientParticulier;
import sopra.vol.MoyenPaiement;
import sopra.vol.dao.IClientRepository;

@Controller
@RequestMapping("/clientParticulier")
public class ClientParticulierController {
	@Autowired
	private IClientRepository clientParticulierRepo;

	public ClientParticulierController() {
		super();

	}

	@GetMapping({ "", "/list" })
	public String list(Model model) {
		model.addAttribute("mesClientsParticuliers", clientParticulierRepo.findAllClientParticulier());

		return "clientParticulier/list";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("monClientParticulier", new ClientParticulier());
		model.addAttribute("civilites", Civilite.values());
		model.addAttribute("moyenPaiements", MoyenPaiement.values());

		return "clientParticulier/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam Long id, Model model) {
		Optional<Client> optClientParticulier = clientParticulierRepo.findById(id);

		if (optClientParticulier.isPresent()) {
			model.addAttribute("monClientParticulier", optClientParticulier.get());
		}

		model.addAttribute("civilites", Civilite.values());

		model.addAttribute("moyenPaiements", MoyenPaiement.values());
		
		return "clientParticulier/form";
	}

	
	@PostMapping("/save")
	public String save(@ModelAttribute("monClientParticulier") @Valid ClientParticulier clientParticulier, BindingResult result, Model model) {

		if (result.hasErrors()) {

			return "clientParticulier/form";
		}

		clientParticulierRepo.save(clientParticulier);

		return "redirect:list";
	}

	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {

		clientParticulierRepo.deleteById(id);

		return "redirect:/clientParticulier/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}
