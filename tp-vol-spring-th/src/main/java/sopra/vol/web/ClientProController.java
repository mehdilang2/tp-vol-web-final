package sopra.vol.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sopra.vol.Client;
import sopra.vol.ClientPro;
import sopra.vol.TypeEntreprise;
import sopra.vol.dao.IClientRepository;

@Controller
@RequestMapping("/clientPro")
public class ClientProController {
	@Autowired
	private IClientRepository clientProRepo;

	public ClientProController() {
		super();

	}

	@GetMapping({ "", "/list" })
	public String list(Model model) {
		model.addAttribute("mesClientPros", clientProRepo.findAllClientPro());

		return "clientPro/list";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("monClientPro", new ClientPro());
		model.addAttribute("typeEntreprises", TypeEntreprise.values());

		return "clientPro/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam Long id, Model model) {
		Optional<Client> optClientPro = clientProRepo.findById(id);

		if (optClientPro.isPresent()) {
			model.addAttribute("monClientPro", optClientPro.get());
		}
		model.addAttribute("typeEntreprises", TypeEntreprise.values());

		return "clientPro/form";
	}

	
	@PostMapping("/save")
	public String save(@ModelAttribute("monClientPro") @Valid ClientPro clientPro, BindingResult result, Model model) {

		if (result.hasErrors()) {

			return "clientPro/form";
		}

		clientProRepo.save(clientPro);

		return "redirect:list";
	}

	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {

		clientProRepo.deleteById(id);

		return "redirect:/clientPro/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}
