package sopra.vol.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sopra.vol.Ville;
import sopra.vol.dao.IVilleRepository;

@Controller
@RequestMapping("/ville")
public class VilleController {
	@Autowired
	private IVilleRepository villeRepo;

	@GetMapping({ "", "/list" })
	public String list(Model model) {
		model.addAttribute("mesVilles", villeRepo.findAll());

		return "ville/list";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("maVille", new Ville());

		return "ville/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam Long id, Model model) {
		Optional<Ville> optVille = villeRepo.findById(id);

		if (optVille.isPresent()) {
			model.addAttribute("monEleve", optVille.get());
		}

		return "ville/form";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute("monVille") @Valid Ville ville, BindingResult result, Model model) {
//		new VilleValidator().validate(ville, result);

		villeRepo.save(ville);

		return "redirect:list";

	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {

		villeRepo.deleteById(id);

		return "redirect:/ville/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}
