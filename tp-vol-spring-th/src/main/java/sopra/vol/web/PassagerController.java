package sopra.vol.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

//import sopra.vol.Adresse;
import sopra.vol.Civilite;
import sopra.vol.Passager;
import sopra.vol.TypePieceIdentite;
import sopra.vol.dao.IPassagerRepository;
import sopra.vol.dao.IReservationRepository;

@Controller
@RequestMapping("/passager")
public class PassagerController {
	@Autowired
	private IPassagerRepository passagerRepo;

	public PassagerController() {
		super();

	}

	@GetMapping({ "", "/list" })
	public String list(Model model) {
		model.addAttribute("mesPassagers", passagerRepo.findAll());

		return "passager/list";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("monPassager", new Passager());
		model.addAttribute("civilites", Civilite.values());
		model.addAttribute("typePieceIdentite", TypePieceIdentite.values());
		//model.addAttribute("principale", new Adresse());
		
		return "passager/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam Long id, Model model) {
		Optional<Passager> optPassager = passagerRepo.findById(id);

		if (optPassager.isPresent()) {
			model.addAttribute("monPassager", optPassager.get());
		}

		model.addAttribute("civilites", Civilite.values());
		model.addAttribute("typePieceIdentite", TypePieceIdentite.values());
		
		return "passager/form";
	}

//	@PostMapping("/save")
//	public String save(@RequestParam Long id, @RequestParam int version, @RequestParam Civilite civilite, @RequestParam String nom, @RequestParam String prenom, @RequestParam int age, @RequestParam String from) {
//
//		Eleve eleve = null;
//
//		if (from.contentEquals("add")) {
//			eleve = new Eleve(civilite, nom, prenom, age);
//		} else {
//			eleve = new Eleve(id, version, civilite, nom, prenom, age);
//		}
//
//		eleveRepo.save(eleve);
//
//		return "redirect:list";
//	}

	@PostMapping("/save")
	public String save(@ModelAttribute("monPassager") @Valid Passager passager, BindingResult result, Model model) {
		//new PassagerValidator().validate(passager, result);

		if (result.hasErrors()) {
			model.addAttribute("civilites", Civilite.values());
			model.addAttribute("typePieceIdentite", TypePieceIdentite.values());

			return "passager/form";
		}

		passagerRepo.save(passager);

		return "redirect:list";
	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {

		passagerRepo.deleteById(id);

		return "redirect:/passager/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}
