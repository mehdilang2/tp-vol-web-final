package sopra.vol.web;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sopra.vol.VoyageVol;
import sopra.vol.dao.IVoyageVolRepository;

@Controller
@RequestMapping("/voyagevol")
public class VoyageVolController {
	@Autowired
	private IVoyageVolRepository voyageVolRepo;

	@GetMapping({ "", "/list" })
	public String list(Model model) {
		model.addAttribute("mesVoyageVol", voyageVolRepo.findAll());

		return "voyagevol/list";
	}

	@GetMapping("/add")
	public String add(Model model) {
		model.addAttribute("monVoyageVol", new VoyageVol());

		return "voyagevol/form";
	}

	@GetMapping("/edit")
	public String edit(@RequestParam Long id, Model model) {
		Optional<VoyageVol> optVoyageVol = voyageVolRepo.findById(id);

		if (optVoyageVol.isPresent()) {
			model.addAttribute("monVoyageVol", optVoyageVol.get());
		}

		return "voyagevol/form";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute("monVoyageVol") @Valid VoyageVol voyageVol, BindingResult result, Model model) {


		voyageVolRepo.save(voyageVol);

		return "redirect:list";

	}

	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Long id) {

		voyageVolRepo.deleteById(id);

		return "redirect:/voyagevol/list";
	}

	@GetMapping("/cancel")
	public String cancel(Model model) {
		return "forward:list";
	}

}