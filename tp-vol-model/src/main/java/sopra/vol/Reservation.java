package sopra.vol;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Reservation {
	@Id
	@GeneratedValue
	private Long id;
	@NotEmpty(message="Ce champ ne peut pas être vide")
	private String numero;
	@NotNull(message="Ce champ ne peut pas être vide")
	private boolean statut;
	@NotNull(message="Ce champ ne peut pas être vide")
	private Float tarif;
	@NotNull(message="Ce champ ne peut pas être vide")
	private Float tauxTVA;
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Future(message = "La date renseignée doit être supérieure à la date d'aujourd'hui")
	@NotNull(message ="La date d''embauche ne peut pas être vide")
	private Date dateReservation;
	@ManyToOne
	@JoinColumn
	private Passager passager;
	@ManyToOne
	@JoinColumn
	private Client client;
	@ManyToOne
	@JoinColumn
	private Voyage voyage;
	@Version
	private int version;
	

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Reservation() {
		super();
	}

	public Reservation(Long id, String numero, boolean statut, Float tarif, Float tauxTVA, Date dateReservation) {
		super();
		this.id = id;
		this.numero = numero;
		this.statut = statut;
		this.tarif = tarif;
		this.tauxTVA = tauxTVA;
		this.dateReservation = dateReservation;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public boolean isStatut() {
		return statut;
	}

	public void setStatut(boolean statut) {
		this.statut = statut;
	}

	public Float getTarif() {
		return tarif;
	}

	public void setTarif(Float tarif) {
		this.tarif = tarif;
	}

	public Float getTauxTVA() {
		return tauxTVA;
	}

	public void setTauxTVA(Float tauxTVA) {
		this.tauxTVA = tauxTVA;
	}

	public Date getDateReservation() {
		return dateReservation;
	}

	public void setDateReservation(Date dateReservation) {
		this.dateReservation = dateReservation;
	}

	public Passager getPassager() {
		return passager;
	}

	public void setPassager(Passager passager) {
		this.passager = passager;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Voyage getVoyage() {
		return voyage;
	}

	public void setVoyage(Voyage voyage) {
		this.voyage = voyage;
	}

}
