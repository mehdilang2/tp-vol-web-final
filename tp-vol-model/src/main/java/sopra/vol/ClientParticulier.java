package sopra.vol;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Version;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


@Entity
@DiscriminatorValue("ClientParticulier")
public class ClientParticulier extends Client {
	
	

public ClientParticulier() {
		super();
	}

		@Version
	private int version;
	@Enumerated(EnumType.STRING)
	private Civilite civilite;
	@NotEmpty(message = "Le nom ne doit pas être nul")
	@Size(min = 2, max = 100, message = "Le nom doit comporter au moins 2 lettres")
	private String nom;
	@NotEmpty(message = "Le prenom ne doit pas être nul")
	@Size(min = 2, max = 100, message = "Le prenom doit comporter au moins 2 lettres")
	private String prenom;

	
	

	public Civilite getCivilite() {
		return civilite;
	}

	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
}
