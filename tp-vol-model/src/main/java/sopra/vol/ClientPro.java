package sopra.vol;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Version;


@Entity
@DiscriminatorValue("ClientPro")
public class ClientPro extends Client {
	@Version
	private int version;
	private String numeroSiret;
	private String nomEntreprise;
	private String numTVA;
	@Enumerated(EnumType.STRING)
	private TypeEntreprise typeEntreprise;


//	public ClientPro(Long id, String mail, String telephone, MoyenPaiement moyenPaiement, Adresse principale,
//			Adresse facturation, List<Reservation> reservations) {
//		super(id, mail, telephone, moyenPaiement, principale, facturation, reservations);
//	}
//
//	public ClientPro(Long id, String mail, String telephone, MoyenPaiement moyenPaiement, Adresse principale,
//			Adresse facturation, List<Reservation> reservations, String numeroSiret, String nomEntreprise,
//			String numTVA, TypeEntreprise typeEntreprise) {
//		super(id, mail, telephone, moyenPaiement, principale, facturation, reservations);
//		this.numeroSiret = numeroSiret;
//		this.nomEntreprise = nomEntreprise;
//		this.numTVA = numTVA;
//		this.typeEntreprise = typeEntreprise;
//	}


	public ClientPro() {
		// TODO Auto-generated constructor stub
	}

	public String getNumeroSiret() {
		return numeroSiret;
	}

	public void setNumeroSiret(String numeroSiret) {
		this.numeroSiret = numeroSiret;
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}

	public String getNumTVA() {
		return numTVA;
	}

	public void setNumTVA(String numTVA) {
		this.numTVA = numTVA;
	}

	public TypeEntreprise getTypeEntreprise() {
		return typeEntreprise;
	}

	public void setTypeEntreprise(TypeEntreprise typeEntreprise) {
		this.typeEntreprise = typeEntreprise;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
