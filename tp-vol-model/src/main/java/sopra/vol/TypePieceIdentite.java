package sopra.vol;

public enum TypePieceIdentite {
	CARTE_IDENTITE("typePI.CARTE_IDENTITE"), PASSEPORT("typePI.PASSEPORT");

	private final String label;

	private TypePieceIdentite(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

}
