package sopra.vol.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sopra.vol.Passager;

public interface IPassagerRepository extends JpaRepository<Passager, Long> {
}
